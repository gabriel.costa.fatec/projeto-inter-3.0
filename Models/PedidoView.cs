using Microsoft.AspNetCore.Mvc.Rendering;

public class PedidoView
{
    public Pedido Pedido { get; set; }
    public List<Produto> ListaProdutos { get; set; }
    public SelectList Clientes { get; internal set; }
    public SelectList Funcionarios { get; internal set; }
    public SelectList Produtos { get; internal set; }
}