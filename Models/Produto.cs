public class Produto
{
    private int sku;
    private int fornecedorId;
    private string categoria;
    private string descricao;
    private string um; 
    private decimal preco;   
    private decimal qtdEstoque;
    private string nomeFuncionario;
    public decimal qtdAdicionada;

    public int SKU
    {
        get { return this.sku; }
        set { this.sku = value; }
    }

    public int FornecedorId
    {
        get { return this.fornecedorId; }
        set { this.fornecedorId = value; }
    }

    public string Categoria
    {
        get { return this.categoria; }
        set { this.categoria = value; }
    }

    public string Descricao
    {
        get { return this.descricao; }
        set { this.descricao = value; }
    }
    
    public string UM
    {
        get { return this.um; }
        set { this.um = value; }
    }

    public decimal Preco
    {
        get { return this.preco; }
        set { this.preco = value; }
    }

    public decimal QtdEstoque
    {
        get { return this.qtdEstoque; }
        set { this.qtdEstoque = value; }
    }

    public string NomeFuncionario
    {
        get { return this.nomeFuncionario; }
        set { this.nomeFuncionario = value; }
    }

    public decimal QtdAdicionada
    {
        get { return this.qtdAdicionada; }
        set { this.qtdAdicionada = value; }
    }
}