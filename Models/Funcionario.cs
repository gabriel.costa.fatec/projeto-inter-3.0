public class Funcionario
{
    private int idFuncionario;
    private string nomeUsuario;
    private string senha;
    private decimal salario;
    private string nome;
    private string cpf;
    private string endereco;
    private string telefone;
    private string email;

    public int IdFuncionario
    {
        get { return this.idFuncionario; }
        set { this.idFuncionario = value; }
    }

    public string NomeUsuario
    {
        get { return this.nomeUsuario; }
        set { this.nomeUsuario = value; }
    }

    public string Senha
    {
        get { return this.senha; }
        set { this.senha = value; }
    }

    public decimal Salario
    {
        get { return this.salario; }
        set { this.salario = value; }
    }

    public string Nome
    {
        get { return this.nome; }
        set { this.nome = value; }
    }

    public string Cpf
    {
        get { return this.cpf; }
        set { this.cpf = value; }
    }

    public string Endereco
    {
        get { return this.endereco; }
        set { this.endereco = value; }
    }

    public string Telefone
    {
        get { return this.telefone; }
        set { this.telefone = value; }
    }

    public string Email
    {
        get { return this.email; }
        set { this.email = value; }
    }
}