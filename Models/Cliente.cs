public class Cliente
{
    private int idCliente;
    private string nome;
    private string cpf;
    private string telefone;
    private string email;
    private string endereco;


    public int IdCliente
    {
        get { return this.idCliente; }
        set { this.idCliente = value; }
    }
    public string Nome
    {
        get { return this.nome; }
        set { this.nome = value; }
    }
    public string CPF
    {
        get { return this.cpf; }
        set { this.cpf = value; }
    }
    public string Telefone
    {
        get { return this.telefone; }
        set { this.telefone = value; }
    }
    public string Email
    {
        get { return this.email; }
        set { this.email = value; }
    }

    public string Endereco
    {
        get { return this.endereco; }
        set { this.endereco = value; }
    }
}