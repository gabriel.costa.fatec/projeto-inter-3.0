public class Fornecedor
{
    private int idFornecedor;
    private string cnpj;
    private string nome;
    private string email;
    private string telefone;


    public int IdFornecedor
    {
        get { return this.idFornecedor; }
        set { this.idFornecedor = value; }
    }
    public string CNPJ
    {
        get { return this.cnpj; }
        set { this.cnpj = value; }
    }
    public string Nome
    {
        get { return this.nome; }
        set { this.nome = value; }
    }
    public string Email
    {
        get { return this.email; }
        set { this.email = value; }
    }

    public string Telefone
    {
        get { return this.telefone; }
        set { this.telefone = value; }
    }
}