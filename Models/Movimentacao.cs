public class Movimentacao
{
    public int idMov;
    public DateTime data;
    public string status;
    public string nomeProduto;
    public decimal qtdMovimento;
    public string funcionario;


    public int IdMov
    {
        get { return this.idMov; }
        set { this.idMov = value; }
    }
    public DateTime Data
    {
        get { return this.data; }
        set { this.data = value; }
    }
    public string Status
    {
        get { return this.status; }
        set { this.status = value; }
    }
    public string NomeProduto
    {
        get { return this.nomeProduto; }
        set { this.nomeProduto = value; }
    }
    public decimal QtdMovimento
    {
        get { return this.qtdMovimento; }
        set { this.qtdMovimento = value; }
    }
    public string Funcionario
    {
        get { return this.funcionario; }
        set { this.funcionario = value; }
    }
}