public class ListaProduto
{
    private int idLista;
	private int pedidoId;
	private int sku;
	private decimal qtd;
	private decimal valor;

    public int IdLista
    {
        get { return this.idLista; }
        set { this.idLista = value; }
    }

    public int PedidoId
    {
        get { return this.pedidoId; }
        set { this.pedidoId = value; }
    }

    public int SKU
    {
        get { return this.sku; }
        set { this.sku = value; }
    }

    public decimal QTD
    {
        get { return this.qtd; }
        set { this.qtd = value; }
    }
    
    public decimal Valor
    {
        get { return this.valor; }
        set { this.valor = value; }
    }
}