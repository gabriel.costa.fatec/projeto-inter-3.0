create database LojaTintas
go

use LojaTintas
go

create table Clientes
(
	idCliente	int				not null	primary key		identity,
	nome		varchar(50)		not null,
	CPF			varchar(14)		not null	unique,
	telefone	varchar(15)		not null,
	email		varchar(50)			null	unique,
	endereco	varchar(200)	not null
)
go

create table Funcionarios
(
	idFuncionario	int				not null	primary key		identity,
	nomeUsuario		varchar(20)		not null	unique,
	senha			varchar(10)		not null,
	salario			decimal(10,2)	null,
	nome			varchar(50)		not null,
	CPF				varchar(14)		not null	unique,
	endereco		varchar(200)	not null,
	telefone		varchar(15)		not null,
	email			varchar(50)			null	unique
)
go

create table Fornecedores
(
	idFornecedor	int			not null	primary key		identity,
	CNPJ			varchar(18)	not null	unique,
	nome			varchar(50)	not null,
	email			varchar(50)	not null	unique,
	telefone		varchar(15)	not null,
)
go

create table Produtos
(
	SKU				int				not null	primary key		identity,
	fornecedorId	int				not null,
	categoria		varchar(50)		not null,
	descricao		varchar(200)	not null	unique,
	UM				varchar(4)		not null,
	preco			decimal(10,2)	not null,
	qtdEstoque		decimal(10,2)	not null,
	foreign key	(fornecedorId)		references	Fornecedores(idFornecedor)
)
go

create table Pedidos
(
	idPedido		int				not null	primary key		identity,
	data			datetime		not null,
	clienteId		int				not null,
	funcionario		varchar(20)		not null,
	valor			decimal(10,2)	not null,
	foreign key (clienteId)			references	Clientes(idCliente),
	foreign key	(funcionario)		references	Funcionarios(nomeUsuario),
)
go

create table Listas_Produtos
(
	idLista			int				not null	primary key		identity,
	pedidoId		int				not null,
	SKU				int				not null,
	qtd				decimal(10,2)	not null,
	valor			decimal(10,2)	not null,
	foreign key (pedidoId)	references Pedidos(idPedido),
	foreign key (SKU)		references Produtos(SKU)
)
go

create table Movimentacoes
(
	idMov			int				not null	primary key		identity,
	data			datetime		not null,
	status			varchar(10)		not null,
	nomeProduto		varchar(200)	not null,
	qtdMovimento	decimal(10,2)	not null,	
	funcionario		varchar(20)		not null,
	foreign key (nomeProduto)		references Produtos(descricao),
	foreign key	(funcionario)		references	Funcionarios(nomeUsuario)
)
go

-- Inserts na tabela Clientes
 INSERT INTO Clientes (nome, CPF, telefone, email, endereco)
 VALUES ('Ana Souza', '111.222.333-44', '(55) 1234-5678', 'ana.souza@email.com', 'Rua D, 123');
 INSERT INTO Clientes (nome, CPF, telefone, email, endereco)
 VALUES ('Carlos Ferreira', '555.444.333-22', '(66) 9876-5432', 'carlos.ferreira@email.com', 'Avenida E, 456');
 INSERT INTO Clientes (nome, CPF, telefone, email, endereco)
 VALUES ('Mariana Santos', '999.888.777-66', '(77) 5555-5555', 'mariana.santos@email.com', 'Rua F, 789');

-- Inserts na tabela Funcionarios
 INSERT INTO Funcionarios (nomeUsuario, senha, salario, nome, CPF, endereco, telefone, email)
 VALUES ('gabriel', 'gabriel123', 2500.00, 'Gabriel Costa', '123.456.789-00', 'Rua A, 123', '(11) 1234-5678', 'gabriel.costa@email.com');
 INSERT INTO Funcionarios (nomeUsuario, senha, salario, nome, CPF, endereco, telefone, email)
 VALUES ('murilo', 'murilo123', 3000.00, 'Murilo Santos', '987.654.321-00', 'Avenida B, 456', '(22) 9876-5432', 'murilo.santos@email.com');
 INSERT INTO Funcionarios (nomeUsuario, senha, salario, nome, CPF, endereco, telefone, email)
 VALUES ('jose', 'jose123', 2800.00, 'José Vitor', '555.555.555-55', 'Rua C, 789', '(33) 5555-5555', 'jose.vitor@email.com');

 -- Inserts na tabela Fornecedores
 INSERT INTO Fornecedores (CNPJ, nome, email, telefone)
 VALUES ('11.222.333/0001-44', 'Casa das Tintas', 'casadastintas@email.com', '(11) 2222-3333');
 INSERT INTO Fornecedores (CNPJ, nome, email, telefone)
 VALUES ('22.333.444/0001-55', 'Cor Certa', 'corcerta@email.com', '(22) 3333-4444');
 INSERT INTO Fornecedores (CNPJ, nome, email, telefone)
 VALUES ('33.444.555/0001-66', 'Materiais para Pintura', 'cores@email.com', '(33) 4444-5555');

 -- Inserts na tabela Produtos
 INSERT INTO Produtos (fornecedorId, categoria, descricao, UM, preco, qtdEstoque)
 VALUES (1, 'Tinta', 'Tinta Branca', 'L', 49.90, 50);
 INSERT INTO Produtos (fornecedorId, categoria, descricao, UM, preco, qtdEstoque)
 VALUES (2, 'Tinta', 'Tinta Azul', 'L', 59.90, 20);
 INSERT INTO Produtos (fornecedorId, categoria, descricao, UM, preco, qtdEstoque)
 VALUES (3, 'Material', 'Rolo', 'UN', 10.90, 10);

 -- Inserts na tabela Pedidos
 INSERT INTO Pedidos (data, clienteId, funcionario, valor)
 VALUES (GETDATE(), 1, 'gabriel', 100.00);
 INSERT INTO Pedidos (data, clienteId, funcionario, valor)
 VALUES (GETDATE(), 2, 'murilo', 200.00);
 INSERT INTO Pedidos (data, clienteId, funcionario, valor)
 VALUES (GETDATE(), 3, 'jose', 180.00);

-- Inserts na tabela Listas_Produtos
INSERT INTO Listas_Produtos (pedidoId, SKU, qtd, valor)
VALUES (1, 1, 10, 49.90);
INSERT INTO Listas_Produtos (pedidoId, SKU, qtd, valor)
VALUES (2, 2, 5, 59.90);
INSERT INTO Listas_Produtos (pedidoId, SKU, qtd, valor)
VALUES (3, 3, 2, 10.90);

-- Inserts na tabela Movimentacoes
INSERT INTO Movimentacoes (data, status, nomeProduto, qtdMovimento, funcionario)
VALUES (GETDATE(), 'Entrada', 'Tinta Branca', 50, 'gabriel');
INSERT INTO Movimentacoes (data, status, nomeProduto, qtdMovimento, funcionario)
VALUES (GETDATE(), 'Entrada', 'Tinta Azul', 20, 'murilo');
INSERT INTO Movimentacoes (data, status, nomeProduto, qtdMovimento, funcionario)
VALUES (GETDATE(), 'Saída', 'Rolo', 2, 'jose');
