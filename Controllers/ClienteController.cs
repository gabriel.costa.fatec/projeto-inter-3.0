using Microsoft.AspNetCore.Mvc;

public class ClienteController : Controller
{
    IClienteRepository clienteRepository;

    public ClienteController(IClienteRepository clienteRepository)
    {
        this.clienteRepository = clienteRepository;
    }
    
    public ActionResult Index()
    {
        ViewBag.Title = "Clientes";
        return View(clienteRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        ViewBag.Title = "Adicionar um cliente";
        return View();
    }

    [HttpPost]
    public ActionResult Create(Cliente model)
    {
        clienteRepository.Create(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        Cliente c = clienteRepository.Read(id);
        if(c != null)
            ViewBag.Title = "Detalhes do cliente";
            return View(c);

        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        clienteRepository.Delete(id);
        return RedirectToAction("Index");
    }

    public ActionResult Update(int id)
    {
        Cliente c = clienteRepository.Read(id);
        if(c != null)
            ViewBag.Title = "Dados do cliente";
            return View(c);

        return NotFound();
    }

    [HttpPost]
    public ActionResult Update(int id, Cliente model)
    {
        clienteRepository.Update(id, model);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        ViewBag.Title = "Clientes";
        return View("Index", clienteRepository.Search(pesquisa));
    }
}

