using Microsoft.AspNetCore.Mvc;

public class FornecedorController : Controller
{
    IFornecedorRepository fornecedorRepository;

    public FornecedorController(IFornecedorRepository fornecedorRepository)
    {
        this.fornecedorRepository = fornecedorRepository;
    }
    
    public ActionResult Index()
    {
        return View(fornecedorRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Fornecedor model)
    {
        fornecedorRepository.Create(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        Fornecedor f = fornecedorRepository.Read(id);
        if(f != null)
            return View(f);

        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        fornecedorRepository.Delete(id);
        return RedirectToAction("Index");
    }

    public ActionResult Update(int id)
    {
        Fornecedor f = fornecedorRepository.Read(id);
        if(f != null)
            return View(f);

        return NotFound();
    }

    [HttpPost]
    public ActionResult Update(int id, Fornecedor model)
    {
        fornecedorRepository.Update(id, model);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", fornecedorRepository.Search(pesquisa));
    }
}