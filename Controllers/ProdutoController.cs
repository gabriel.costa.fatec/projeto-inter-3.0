using Microsoft.AspNetCore.Mvc;

public class ProdutoController : Controller
{
    IProdutoRepository produtoRepository;

    public ProdutoController(IProdutoRepository produtoRepository)
    {
        this.produtoRepository = produtoRepository;
    }

    public ActionResult Index()
    {
        return View(produtoRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Produto model)
    {
        produtoRepository.Create(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        Produto p = produtoRepository.Read(id);
        if(p != null)
            return View(p);

        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        produtoRepository.Delete(id);
        return RedirectToAction("Index");
    }

    public ActionResult Update(int id)
    {
        Produto p = produtoRepository.Read(id);
        if(p != null)
            return View(p);

        return NotFound();
    }

    [HttpPost]
    public ActionResult Update(int id, Produto model)
    {
        produtoRepository.Update(id, model);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", produtoRepository.Search(pesquisa));
    }
}