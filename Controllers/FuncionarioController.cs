using Microsoft.AspNetCore.Mvc;

public class FuncionarioController : Controller
{
    IFuncionarioRepository funcionarioRepository;

    public FuncionarioController(IFuncionarioRepository funcionarioRepository)
    {
        this.funcionarioRepository = funcionarioRepository;
    }
    
    public ActionResult Index()
    {
        return View(funcionarioRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Funcionario model)
    {
        funcionarioRepository.Create(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        Funcionario func = funcionarioRepository.Read(id);
        if(func != null)
            ViewBag.Title = "Dados do Funcionário";
            return View(func);

        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        funcionarioRepository.Delete(id);
        return RedirectToAction("Index");
    }

    public ActionResult Update(int id)
    {
        Funcionario func = funcionarioRepository.Read(id);
        if(func != null)
            return View(func);

        return NotFound();
    }

    [HttpPost]
    public ActionResult Update(int id, Funcionario model)
    {
        funcionarioRepository.Update(id, model);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", funcionarioRepository.Search(pesquisa));
    }
}