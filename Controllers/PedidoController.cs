using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;

public class PedidoController : Controller
{
    IPedidoRepository pedidoRepository;
    IProdutoRepository produtoRepository;
    IClienteRepository clienteRepository;
    private SqlConnection conn;

    public PedidoController(IPedidoRepository pedidoRepository, IProdutoRepository produtoRepository, IClienteRepository clienteRepository)
    {
        this.pedidoRepository = pedidoRepository;
        this.produtoRepository = produtoRepository;
        this.clienteRepository = clienteRepository;
    }
    
    public ActionResult Index()
    {
        return View(pedidoRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        
        var clientes = clienteRepository.Read().ToList();
        var produtos = produtoRepository.Read().ToList();
        
        var view = new PedidoView
        {
            Pedido = new Pedido(),
            ListaProdutos = new List<Produto>(),
            Clientes = new SelectList(clientes),
            Produtos = new SelectList(produtos)
        };

        return View(view);
    }

    [HttpPost]
    public ActionResult Create(PedidoView model)
    {
        if(ModelState.IsValid)
        {
            var pedido = model.Pedido;
            pedidoRepository.Create(pedido);

            var listaProdutos = model.ListaProdutos;
            foreach (var produto in listaProdutos)
            {
                produtoRepository.Create(produto);
            }

            return RedirectToAction("Index");
        }

        return RedirectToAction("Create");
    }

    public ActionResult Details(int id)
    {
        Pedido p = pedidoRepository.Read(id);
        if(p != null)
            return View(p);

        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        pedidoRepository.Delete(id);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", pedidoRepository.Search(pesquisa));
    }
}