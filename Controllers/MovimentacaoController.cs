using Microsoft.AspNetCore.Mvc;

public class MovimentacaoController : Controller
{
    IMovimentacaoRepository movimentacaoRepository;

    public MovimentacaoController(IMovimentacaoRepository movimentacaoRepository)
    {
        this.movimentacaoRepository = movimentacaoRepository;
    }
    
    public ActionResult Index()
    {
        return View(movimentacaoRepository.Read());
    }

    public ActionResult Details(int id)
    {
        Movimentacao m = movimentacaoRepository.Read(id);
        if(m != null)
            return View(m);

        return NotFound();
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", movimentacaoRepository.Search(pesquisa));
    }
}
