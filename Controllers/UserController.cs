using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

public class UserController : Controller
{
    IUserRepository repository;

    public UserController(IUserRepository repository) 
    {
        this.repository = repository;
    }

    [HttpGet]
    public ActionResult Login()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Login(IFormCollection form)
    {
        string nomeUsuario = form["nomeUsuario"];
        string senha = form["senha"];


        Console.WriteLine("Controller");
        Console.WriteLine(nomeUsuario);
        Console.WriteLine(senha);

        var user = repository.Login(nomeUsuario!, senha!);

        if(user == null)
        {
            ViewBag.Error = "Usuário/Senha inválidos";
            return View();
        }

        HttpContext.Session.SetString("user", JsonSerializer.Serialize(user));

        return RedirectToAction("Index", "Main");
    }

    [HttpGet]
    public ActionResult Logout()
    {
        HttpContext.Session.Clear();
        return RedirectToAction("Login");
    }
}