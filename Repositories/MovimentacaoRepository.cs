using Microsoft.Data.SqlClient;

public class MovimentacaoRepository : Database, IMovimentacaoRepository
{

    public List<Movimentacao> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Movimentacoes";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Movimentacao> movimentacoes = new List<Movimentacao>();

        while(reader.Read())
        {
            Movimentacao m = new Movimentacao();
            m.IdMov = reader.GetInt32(0);
            m.Data = reader.GetDateTime(1);
            m.Status = reader.GetString(2);
            m.NomeProduto = reader.GetString(3);
            m.QtdMovimento = reader.GetDecimal(4);
            m.Funcionario = reader.GetString(5);

            movimentacoes.Add(m);
        }

        return movimentacoes;

    }

    public Movimentacao Read(int id)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Movimentacoes WHERE idMov = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Movimentacao m = new Movimentacao();
            m.IdMov = reader.GetInt32(0);
            m.Data = reader.GetDateTime(1);
            m.Status = reader.GetString(2);
            m.NomeProduto = reader.GetString(3);
            m.QtdMovimento = reader.GetDecimal(4);
            m.Funcionario = reader.GetString(5);
            
            return m;
        }

        return null;

    }

    public List<Movimentacao> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Movimentacoes WHERE SKU like @pesquisa or status like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Movimentacao> movimentacoes = new List<Movimentacao>();

        while(reader.Read())
        {
            Movimentacao m = new Movimentacao();
            m.IdMov = reader.GetInt32(0);
            m.Data = reader.GetDateTime(1);
            m.Status = reader.GetString(2);
            m.NomeProduto = reader.GetString(3);
            m.QtdMovimento = reader.GetDecimal(4);
            m.Funcionario = reader.GetString(5);

            movimentacoes.Add(m);
        }

        return movimentacoes;

    }
}
