public interface IMovimentacaoRepository
{
    List<Movimentacao> Read();
    Movimentacao Read(int id);
    List<Movimentacao> Search(string pesquisa);
}