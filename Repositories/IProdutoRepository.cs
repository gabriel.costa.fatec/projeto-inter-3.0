public interface IProdutoRepository
{
    List<Produto> Read();
    void Create(Produto produto);
    Produto Read(int id);
    void Update(int id, Produto produto);
    void Delete(int id);
    List<Produto> Search(string pesquisa);
}