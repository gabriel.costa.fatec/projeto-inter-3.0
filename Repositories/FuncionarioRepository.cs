using Microsoft.Data.SqlClient;

public class FuncionarioRepository : Database, IFuncionarioRepository
{
    public void Create(Funcionario funcionario)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Funcionarios VALUES (@nomeUsuario, @senha, @salario, @nome, @cpf, @endereco, @telefone, @email)";

        cmd.Parameters.AddWithValue("@nomeUsuario", funcionario.NomeUsuario);
        cmd.Parameters.AddWithValue("@senha", funcionario.Senha);
        cmd.Parameters.AddWithValue("@salario", funcionario.Salario);
        cmd.Parameters.AddWithValue("@nome", funcionario.Nome);
        cmd.Parameters.AddWithValue("@cpf", funcionario.Cpf);
        cmd.Parameters.AddWithValue("@endereco", funcionario.Endereco);
        cmd.Parameters.AddWithValue("@telefone", funcionario.Telefone);
        cmd.Parameters.AddWithValue("@email", funcionario.Email);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Funcionarios WHERE idFuncionario = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }

    public List<Funcionario> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Funcionarios";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Funcionario> funcionarios = new List<Funcionario>();

        while(reader.Read())
        {
            Funcionario func = new Funcionario();
            func.Nome = reader.GetString(4);
            func.Telefone = reader.GetString(7);
            func.Email = reader.GetString(8);

            funcionarios.Add(func);
        }

        return funcionarios;

    }

    public Funcionario Read(int id)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Funcionarios WHERE idFuncionario = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Funcionario func = new Funcionario();
            func.IdFuncionario = reader.GetInt32(0);
            func.NomeUsuario = reader.GetString(1);
            func.Nome = reader.GetString(4);
            func.Cpf = reader.GetString(5);
            func.Endereco = reader.GetString(6);
            func.Telefone = reader.GetString(7);
            func.Email = reader.GetString(8);

            return func;
        }

        return null;

    }

    public List<Funcionario> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Funcionarios WHERE Nome like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Funcionario> funcionarios = new List<Funcionario>();

        while(reader.Read())
        {
            Funcionario func = new Funcionario();
            func.Nome = reader.GetString(4);
            func.Telefone = reader.GetString(7);
            func.Email = reader.GetString(8);

            funcionarios.Add(func);
        }

        return funcionarios;

    }

    void IFuncionarioRepository.Update(int id, Funcionario funcionario)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "UPDATE Funcionarios SET nomeUsuario = @nomeUsuario, nome = @nome, CPF = @cpf, endereco = @endereco, telefone = @telefone, email = @email WHERE idFuncionario = @id";
        cmd.Parameters.AddWithValue("@nomeUsuario", funcionario.NomeUsuario);
        cmd.Parameters.AddWithValue("@nome", funcionario.Nome);
        cmd.Parameters.AddWithValue("@cpf", funcionario.Cpf);
        cmd.Parameters.AddWithValue("@endereco", funcionario.Endereco);
        cmd.Parameters.AddWithValue("@telefone", funcionario.Telefone);
        cmd.Parameters.AddWithValue("@email", funcionario.Email);
        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }
}