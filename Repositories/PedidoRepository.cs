using Microsoft.Data.SqlClient;

public class PedidoRepository : Database, IPedidoRepository
{
    public void Create(Pedido pedido)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Pedidos VALUES (@data, @clienteId, @funcionario, @valor)";

        var dataAtual = DateTime.UtcNow;
        var valorInicial = 0;
        var clienteId = 6;
        var funcionario = "rafa.maria";

        cmd.Parameters.AddWithValue("@data", dataAtual);
        cmd.Parameters.AddWithValue("@clienteId", clienteId);
        cmd.Parameters.AddWithValue("@funcionario", funcionario);
        cmd.Parameters.AddWithValue("@valor", valorInicial);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Pedidos WHERE idPedido = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }

    public List<Pedido> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Pedidos";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Pedido> pedidos = new List<Pedido>();

        while(reader.Read())
        {
            Pedido p = new Pedido();
            p.IdPedido = reader.GetInt32(0);
            p.Data = reader.GetDateTime(1);
            p.ClienteId = reader.GetInt32(2);
            p.Funcionario = reader.GetString(3);
            p.Valor = reader.GetDecimal(4);

            pedidos.Add(p);
        }

        return pedidos;

    }

    public Pedido Read(int id)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Pedidos WHERE idPedido = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Pedido p = new Pedido();
            p.IdPedido = reader.GetInt32(0);
            p.Data = reader.GetDateTime(1);
            p.ClienteId = reader.GetInt32(2);
            p.Funcionario = reader.GetString(3);
            p.Valor = reader.GetDecimal(4);
            
            return p;
        }

        return null;

    }

    public List<Pedido> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Pedidos WHERE idPedido like @pesquisa or funcionario like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Pedido> pedidos = new List<Pedido>();

        while(reader.Read())
        {
            Pedido p = new Pedido();
            p.IdPedido = reader.GetInt32(0);
            p.Data = reader.GetDateTime(1);
            p.ClienteId = reader.GetInt32(2);
            p.Funcionario = reader.GetString(3);
            p.Valor = reader.GetDecimal(4);

            pedidos.Add(p);
        }

        return pedidos;

    }
}