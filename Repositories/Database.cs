using Microsoft.Data.SqlClient;

public abstract class Database : IDisposable
{
    protected SqlConnection conn;

    public Database()
    {
        string connectionString = "Data Source=localhost\\SQLEXPRESS; Initial Catalog=LojaTintas; Integrated Security=True; TrustServerCertificate=true";

        conn = new SqlConnection(connectionString);
        conn.Open();

        Console.WriteLine("Conexão aberta");
    }

    public void Dispose() // Fecha a conexão
    {
        conn.Close();
        Console.WriteLine("Conexão fechada");
    }
}
