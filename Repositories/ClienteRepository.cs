using Microsoft.Data.SqlClient;

public class ClienteRepository : Database, IClienteRepository
{
    public void Create(Cliente cliente)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Clientes VALUES (@nome, @cpf, @telefone, @email, @endereco)";

        cmd.Parameters.AddWithValue("@nome", cliente.Nome);
        cmd.Parameters.AddWithValue("@cpf", cliente.CPF);
        cmd.Parameters.AddWithValue("@telefone", cliente.Telefone);
        cmd.Parameters.AddWithValue("@email", cliente.Email);
        cmd.Parameters.AddWithValue("@endereco", cliente.Endereco);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Clientes WHERE idCliente = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }

    public List<Cliente> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Clientes";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Cliente> clientes = new List<Cliente>();

        while(reader.Read())
        {
            Cliente c = new Cliente();
            c.IdCliente = reader.GetInt32(0);
            c.Nome = reader.GetString(1);
            c.CPF = reader.GetString(2);
            c.Telefone = reader.GetString(3);
            c.Email = reader.GetString(4);
            c.Endereco = reader.GetString(5);

            clientes.Add(c);
        }

        return clientes;

    }

    public Cliente Read(int id)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Clientes WHERE idCliente = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Cliente c = new Cliente();
            c.IdCliente = reader.GetInt32(0);
            c.Nome = reader.GetString(1);
            c.CPF = reader.GetString(2);
            c.Telefone = reader.GetString(3);
            c.Email = reader.GetString(4);
            c.Endereco = reader.GetString(5);

            return c;
        }

        return null;

    }

    public List<Cliente> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Clientes WHERE Nome like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Cliente> clientes = new List<Cliente>();

        while(reader.Read())
        {
            Cliente c = new Cliente();
            c.IdCliente = reader.GetInt32(0);
            c.Nome = reader.GetString(1);
            c.Telefone = reader.GetString(3);
            c.Email = reader.GetString(4);
            c.Endereco = reader.GetString(5);

            clientes.Add(c);
        }

        return clientes;

    }

    void IClienteRepository.Update(int id, Cliente cliente)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "UPDATE Clientes SET nome = @nome, CPF = @cpf, telefone = @telefone, email = @email, endereco = @endereco WHERE idCliente = @id";
        cmd.Parameters.AddWithValue("@nome", cliente.Nome);
        cmd.Parameters.AddWithValue("@cpf", cliente.CPF);
        cmd.Parameters.AddWithValue("@telefone", cliente.Telefone);
        cmd.Parameters.AddWithValue("@email", cliente.Email);
        cmd.Parameters.AddWithValue("@endereco", cliente.Endereco);
        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }
}