using Microsoft.Data.SqlClient;

public class UserRepository : Database, IUserRepository
{
    public User Login(string nomeUsuario, string senha)
    {

        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT nomeUsuario, senha, nome FROM Funcionarios WHERE nomeUsuario = @nomeUsuario and senha = @senha";

        Console.WriteLine("Repository");
        Console.WriteLine(nomeUsuario);
        Console.WriteLine(senha);

        cmd.Parameters.AddWithValue("@nomeUsuario", nomeUsuario);
        cmd.Parameters.AddWithValue("@senha", senha);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            User u = new User();
            u.NomeUsuario = reader.GetString(0);
            u.Senha = reader.GetString(1);
            u.NomeFuncionario = reader.GetString(2);

            return u;
        }

        return null;
    }
}