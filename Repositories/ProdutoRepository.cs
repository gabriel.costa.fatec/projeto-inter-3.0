using Microsoft.Data.SqlClient;

public class ProdutoRepository : Database, IProdutoRepository
{
    public void Create(Produto produto)
    { 
        SqlCommand cmd1 = new SqlCommand();
        cmd1.Connection = conn;
        cmd1.CommandText = "INSERT INTO Produtos VALUES (@fornecedorId, @categoria, @descricao, @UM, @preco,  @qtdEstoque)";

        cmd1.Parameters.AddWithValue("@fornecedorId", produto.FornecedorId);
        cmd1.Parameters.AddWithValue("@categoria", produto.Categoria);
        cmd1.Parameters.AddWithValue("@descricao", produto.Descricao);
        cmd1.Parameters.AddWithValue("@UM", produto.UM);
        cmd1.Parameters.AddWithValue("@preco", produto.Preco);
        cmd1.Parameters.AddWithValue("@qtdEstoque", produto.QtdEstoque);

        cmd1.ExecuteNonQuery();

        var status = "Entrada";

        TimeZoneInfo timeZone= TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time");
        DateTime dataAtual = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZone);

        SqlCommand cmd2 = new SqlCommand();
        cmd2.Connection = conn;
        cmd2.CommandText = "INSERT INTO Movimentacoes VALUES (@data, @status, @nomeProduto, @qtdMovimento, @funcionario)";

        cmd2.Parameters.AddWithValue("@data", dataAtual);
        cmd2.Parameters.AddWithValue("@status", status);
        cmd2.Parameters.AddWithValue("@nomeProduto", produto.Descricao);
        cmd2.Parameters.AddWithValue("@qtdMovimento", produto.QtdEstoque);
        cmd2.Parameters.AddWithValue("@funcionario", produto.NomeFuncionario);

        cmd2.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Produtos WHERE SKU = @id";

        Console.WriteLine(id);

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();
    }

    public List<Produto> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produtos";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Produto> produtos = new List<Produto>();

        while(reader.Read())
        {
            Produto p = new Produto();
            p.SKU = reader.GetInt32(0);
            p.FornecedorId = reader.GetInt32(1);
            p.Categoria = reader.GetString(2);
            p.Descricao = reader.GetString(3);
            p.UM = reader.GetString(4);
            p.Preco = reader.GetDecimal(5);
            p.QtdEstoque = reader.GetDecimal(6);

            produtos.Add(p);
        }

        return produtos;
    }

    public Produto Read(int id)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produtos WHERE SKU = @id";
        
        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Produto p = new Produto();
            p.SKU = reader.GetInt32(0);
            p.FornecedorId = reader.GetInt32(1);
            p.Categoria = reader.GetString(2);
            p.Descricao = reader.GetString(3);
            p.UM = reader.GetString(4);
            p.Preco = reader.GetDecimal(5);
            p.QtdEstoque = reader.GetDecimal(6);

            return p;
        }

        return null;
    }

    public List<Produto> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produtos WHERE descricao like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Produto> produtos = new List<Produto>();

        while(reader.Read())
        {
            Produto p = new Produto();
            p.SKU = reader.GetInt32(0);
            p.FornecedorId = reader.GetInt32(1);
            p.Categoria = reader.GetString(2);
            p.Descricao = reader.GetString(3);
            p.UM = reader.GetString(4);
            p.Preco = reader.GetDecimal(5);
            p.QtdEstoque = reader.GetDecimal(6);

            produtos.Add(p);
        }

        return produtos;
    }

    public void Update(int id, Produto produto)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "UPDATE Produtos SET preco = @preco, qtdEstoque = @qtdEstoque WHERE SKU = @id";
        
        var nomeFuncionario = produto.NomeFuncionario;
        var nomeProduto = produto.Descricao;
        var novaQtd = produto.QtdAdicionada + produto.QtdEstoque;

        cmd.Parameters.AddWithValue("@preco", produto.Preco);
        cmd.Parameters.AddWithValue("@qtdEstoque", novaQtd);
        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

        var status = "Entrada";
        var dataAtual = DateTime.UtcNow;

        SqlCommand cmd2 = new SqlCommand();
        cmd2.Connection = conn;
        cmd2.CommandText = "INSERT INTO Movimentacoes VALUES (@data, @status, @nomeProduto, @qtdMovimento, @funcionario)";

        cmd2.Parameters.AddWithValue("@data", dataAtual);
        cmd2.Parameters.AddWithValue("@status", status);
        cmd2.Parameters.AddWithValue("@nomeProduto", nomeProduto);
        cmd2.Parameters.AddWithValue("@qtdMovimento", produto.qtdAdicionada);
        cmd2.Parameters.AddWithValue("@funcionario", nomeFuncionario);

        cmd2.ExecuteNonQuery();
    }
}